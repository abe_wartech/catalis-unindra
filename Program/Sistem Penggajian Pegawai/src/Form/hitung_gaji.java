/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Form;

import java.sql.*;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import java.awt.event.KeyEvent;
import Koneksi.koneksi;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import javax.swing.JSpinner;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.view.JasperViewer;
import java.util.HashMap;

/**
 *
 * @author RERI
 */
public class hitung_gaji extends javax.swing.JFrame {

    public String bulan, id_absensi, id_pegawai, nama_pegawai, jenis_kelamin, notelp, jabatan, kehadiran, tidakhadir, jamlembur;
    public String id_jabatan, nama_jabatan, gaji_pokok;
    private Connection conn = new koneksi().connect();
    private DefaultTableModel tabmode;
    private SimpleDateFormat sdf;

    /**
     * Creates new form pegawai
     */
    public hitung_gaji() {
        initComponents();
        kosong();
        aktif();
        autonumber();
    }

    protected void aktif() {
        jtgl.setEditor(new JSpinner.DateEditor(jtgl, "yyyy-MM-dd, HH:mm:ss"));
        Calendar cal = Calendar.getInstance();
        Date startTime = cal.getTime();
        Date endTime = cal.getTime();
        kalender.setDate(new Date());
        Object[] Baris = {"ID Slip Gaji", "Bulan", "Nama Pegawai", "Jabatan", "Gaji Pokok", "Kehadiran", "Tidak Hadir", "Total Jam Lembur", "Total Upah Lembur", "Total Gaji"};
        tabmode = new DefaultTableModel(null, Baris);
        tblgaji.setModel(tabmode);
        jtgl.setValue(startTime);
    }

    protected void kosong() {
        txtidgaji.setEditable(false);
        txtidabsensi.setEditable(false);
        txtidpegawai.setEditable(false);
        txtnama.setEditable(false);
        txtnotelp.setEditable(false);
        txtjabatan.setEditable(false);
        rlaki.setEnabled(false);
        rperempuan.setEnabled(false);
        txtidjabatan.setEditable(false);
        txtnamajabatan.setEditable(false);
        txtgajipokok.setEditable(false);
        txtkehadiran.setEditable(false);
        txttidakhadir.setEditable(false);
        txtjamlembur.setEditable(false);
        txttotalupahlembur.setEditable(false);
        txttotalgaji.setEditable(false);
        txtidabsensi.setText("");
        txtidpegawai.setText("");
        txtnama.setText("");
        txtnotelp.setText("");
        txtjabatan.setText("");
        buttonGroup1.clearSelection();
        txtidjabatan.setText("");
        txtnamajabatan.setText("");
        txtgajipokok.setText("");
        txtkehadiran.setText("");
        txttidakhadir.setText("");
        txtjamlembur.setText("");
        txttotalupahlembur.setText("");
        txttotalgaji.setText("");
    }

    protected void autonumber() {
        try {
            String sql = "SELECT id_slip_gaji FROM slip_gaji order by id_slip_gaji asc";
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery(sql);
            txtidgaji.setText("IG0001");
            while (rs.next()) {
                String id = rs.getString("id_slip_gaji").substring(2);
                int AN = Integer.parseInt(id) + 1;
                String Nol = "";
                if (AN < 10) {
                    Nol = "000";
                } else if (AN < 100) {
                    Nol = "00";
                } else if (AN < 1000) {
                    Nol = "0";
                } else if (AN < 10000) {
                    Nol = "";
                }
                txtidgaji.setText("IG" + Nol + AN);
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Auto Number Gagal" + e);
        }
    }

    protected void tambah() {
        try {
            String id = txtidgaji.getText();
            sdf = new SimpleDateFormat("MMMM yyyy");
            String bulan1 = (sdf.format(kalender.getDate()));
            String nama = txtnama.getText();
            String jabatan1 = txtjabatan.getText();
            String gajipokok = txtgajipokok.getText();
            String kehadiran1 = txtkehadiran.getText();
            String tidak_hadir = txttidakhadir.getText();
            String total_jam_lembur = txtjamlembur.getText();
            String total_upah_lembur = txttotalupahlembur.getText();
            String total_gaji = txttotalgaji.getText();

            tabmode.addRow(new Object[]{id, bulan, nama, jabatan1, gajipokok, kehadiran1, tidak_hadir, total_jam_lembur, total_upah_lembur, total_gaji});
            tblgaji.setModel(tabmode);
        } catch (Exception e) {
            System.out.println("Error : " + e);
        }
    }

    public void itemTerpilih1() {
        popup_absensi Pa = new popup_absensi();
        Pa.abs = this;
        try {
            sdf = new SimpleDateFormat("MMMM yyyy");
            java.util.Date tgl = sdf.parse(bulan);
            kalender.setDate(tgl);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        txtidabsensi.setText(id_absensi);
        txtidpegawai.setText(id_pegawai);
        txtnama.setText(nama_pegawai);
        if ("Laki-Laki".equals(jenis_kelamin)) {
            rlaki.setSelected(true);
        } else {
            rperempuan.setSelected(true);
        }
        txtnotelp.setText(notelp);
        txtjabatan.setText(jabatan);
        txtkehadiran.setText(kehadiran);
        txttidakhadir.setText(tidakhadir);
        txtjamlembur.setText(jamlembur);
        format_hari();
    }

    public void itemTerpilih2() {
        popup_jabatan2 Pj = new popup_jabatan2();
        Pj.jbtn2 = this;
        txtidjabatan.setText(id_jabatan);
        txtnamajabatan.setText(nama_jabatan);
        txtgajipokok.setText(gaji_pokok);
        hitung_gaji();
        format_uang();
        txttotalgaji.requestFocus();
    }

    public void format_uang() {
        try {
            double gajipokok = Double.parseDouble(txtgajipokok.getText());
            double upah_lembur = Double.parseDouble(txttotalupahlembur.getText());
            double total_gaji = Double.parseDouble(txttotalgaji.getText());
            DecimalFormat df = (DecimalFormat) DecimalFormat.getCurrencyInstance();
            DecimalFormatSymbols dfs = new DecimalFormatSymbols();
            dfs.setCurrencySymbol("");
            dfs.setMonetaryDecimalSeparator(',');
            dfs.setGroupingSeparator('.');
            df.setDecimalFormatSymbols(dfs);
            String hslgajipokok = "Rp. " + df.format(gajipokok);
            String hslupah_lembur = "Rp. " + df.format(upah_lembur);
            String hsltotal_gaji = "Rp. " + df.format(total_gaji);
            txtgajipokok.setText(hslgajipokok);
            txttotalupahlembur.setText(hslupah_lembur);
            txttotalgaji.setText(hsltotal_gaji);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Pengisian gaji tidak boleh kosong");
        }
    }

    public void format_hari() {
        try {
            int kehadiran = Integer.parseInt(txtkehadiran.getText());
            int tidak_hadir = Integer.parseInt(txttidakhadir.getText());
            String hslkehadiran = kehadiran + " Hari";
            String hsltidak_hadir = tidak_hadir + " Hari";
            txtkehadiran.setText(hslkehadiran);
            txttidakhadir.setText(hsltidak_hadir);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Pengisian gaji tidak boleh kosong");
        }
    }

    public void format_jam() {
        try {
            int lembur = Integer.parseInt(txtjamlembur.getText());
            String hsllembur = lembur + " Jam";
            txtjamlembur.setText(hsllembur);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Pengisian gaji tidak boleh kosong");
        }
    }

    protected void hitung_gaji() {
        int gaji_pokok = Integer.parseInt(txtgajipokok.getText());
        int lembur = Integer.parseInt(txtjamlembur.getText());
        int gaji_per_jam = gaji_pokok * 1 / 173;
        int upahlembur1 = gaji_per_jam * 3 / 2;
        int upahlembur2 = gaji_per_jam * (lembur - 1) * 2;
        int total_upah_lembur = upahlembur1 + upahlembur2;
        int total_gaji = gaji_pokok + total_upah_lembur;
        if (txtjamlembur.getText() == "1") {
            txttotalupahlembur.setText(String.valueOf(upahlembur1));
        } else {
            txttotalupahlembur.setText(String.valueOf(total_upah_lembur));
        }
        txttotalgaji.setText(String.valueOf(total_gaji));
    }

    protected void simpan() {
        SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd, HH:mm:ss");
        SimpleDateFormat sdf2 = new SimpleDateFormat("MMMM yyyy");
        String tanggal = sdf1.format(jtgl.getValue());
        String bulan1 = sdf2.format(kalender.getDate());
        String sql = "insert into slip_gaji values (?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

        try {

            PreparedStatement stat = conn.prepareStatement(sql);
            stat.setString(1, tanggal);
            stat.setString(2, txtidgaji.getText());
            stat.setString(3, bulan1);
            stat.setString(4, txtidabsensi.getText());
            stat.setString(5, txtidpegawai.getText());
            stat.setString(6, txtnama.getText());
            stat.setString(7, txtidjabatan.getText());
            stat.setString(8, txtnamajabatan.getText());
            stat.setString(9, txtgajipokok.getText());
            stat.setString(10, txtkehadiran.getText());
            stat.setString(11, txttidakhadir.getText());
            stat.setString(12, txtjamlembur.getText());
            stat.setString(13, txttotalupahlembur.getText());
            stat.setString(14, txttotalgaji.getText());

            stat.executeUpdate();

            int t = tblgaji.getRowCount();
            for (int i = 0; i < t; i++) {
            }
            JOptionPane.showMessageDialog(null, "data berhasil disimpan");
            cetak();
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "data gagal disimpan" + e);
        }
        kosong();
        aktif();
        autonumber();
    }

    public void cetak() {
        try {
            String path = "./src/Report/slip_gaji.jasper";
            HashMap parameter = new HashMap();
            parameter.put("id_slip_gaji", txtidgaji.getText());
            JasperPrint print = JasperFillManager.fillReport(path, parameter, conn);
            JasperViewer.viewReport(print, false);
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, "DokumenTidak Ada " + ex);
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        javax.swing.JPanel jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        txtidpegawai = new javax.swing.JTextField();
        txtnama = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        rlaki = new javax.swing.JRadioButton();
        rperempuan = new javax.swing.JRadioButton();
        txtjabatan = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        txtnotelp = new javax.swing.JTextField();
        jLabel11 = new javax.swing.JLabel();
        txtidabsensi = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        kalender = new com.toedter.calendar.JDateChooser();
        bcaria = new javax.swing.JButton();
        jPanel4 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tblgaji = new javax.swing.JTable();
        bsimpan = new javax.swing.JButton();
        bhapus = new javax.swing.JButton();
        bbatal = new javax.swing.JButton();
        bcetak = new javax.swing.JButton();
        bkeluar = new javax.swing.JButton();
        jPanel7 = new javax.swing.JPanel();
        jLabel22 = new javax.swing.JLabel();
        jLabel23 = new javax.swing.JLabel();
        txtnamajabatan = new javax.swing.JTextField();
        txtgajipokok = new javax.swing.JTextField();
        jLabel27 = new javax.swing.JLabel();
        jLabel28 = new javax.swing.JLabel();
        jLabel29 = new javax.swing.JLabel();
        txtkehadiran = new javax.swing.JTextField();
        jLabel30 = new javax.swing.JLabel();
        txttidakhadir = new javax.swing.JTextField();
        txtjamlembur = new javax.swing.JTextField();
        txtidjabatan = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        txttotalgaji = new javax.swing.JTextField();
        bcarij = new javax.swing.JButton();
        jLabel24 = new javax.swing.JLabel();
        txttotalupahlembur = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        txtidgaji = new javax.swing.JTextField();
        jtgl = new javax.swing.JSpinner();
        jLabel9 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setMaximumSize(new java.awt.Dimension(976, 488));
        setMinimumSize(new java.awt.Dimension(976, 488));
        setResizable(false);
        setSize(new java.awt.Dimension(0, 0));

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));

        jPanel2.setBackground(new java.awt.Color(0, 51, 153));

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("HITUNG GAJI");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(586, 586, 586)
                .addComponent(jLabel1)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel3.setBackground(new java.awt.Color(255, 255, 255));
        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)), "Data Absensi"));
        jPanel3.setToolTipText("");

        jLabel2.setText("ID Pegawai");

        jLabel5.setText("Nama Pegawai");

        jLabel6.setText("Jenis Kelamin");

        txtidpegawai.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtidpegawaiActionPerformed(evt);
            }
        });

        jLabel7.setText("Jabatan");

        rlaki.setBackground(new java.awt.Color(255, 255, 255));
        buttonGroup1.add(rlaki);
        rlaki.setText("Laki - Laki");

        rperempuan.setBackground(new java.awt.Color(255, 255, 255));
        buttonGroup1.add(rperempuan);
        rperempuan.setText("Perempuan");

        jLabel4.setText("No. Telepon");

        jLabel11.setText("ID Absensi");

        jLabel10.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel10.setText("Bulan ");

        kalender.setDateFormatString("MMMM yyyy");

        bcaria.setText("Cari");
        bcaria.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bcariaActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel5)
                    .addComponent(jLabel2)
                    .addComponent(jLabel6)
                    .addComponent(jLabel7)
                    .addComponent(jLabel4)
                    .addComponent(jLabel11)
                    .addComponent(jLabel10))
                .addGap(42, 42, 42)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(rlaki)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(rperempuan)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(txtnama)
                    .addComponent(txtjabatan)
                    .addComponent(txtnotelp)
                    .addComponent(kalender, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                        .addComponent(txtidabsensi)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(bcaria))
                    .addComponent(txtidpegawai))
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel10)
                    .addComponent(kalender, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel11)
                    .addComponent(txtidabsensi, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bcaria))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(txtidpegawai, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(txtnama, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(rlaki)
                    .addComponent(rperempuan))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(txtnotelp, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(txtjabatan, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel4.setBackground(new java.awt.Color(255, 255, 255));
        jPanel4.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)), "Tabel Absensi"));

        tblgaji.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tblgaji.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tblgajiMouseClicked(evt);
            }
        });
        tblgaji.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tblgajiKeyPressed(evt);
            }
        });
        jScrollPane2.setViewportView(tblgaji);

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 970, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                .addContainerGap())
        );

        bsimpan.setText("Tambah");
        bsimpan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bsimpanActionPerformed(evt);
            }
        });

        bhapus.setText("Hapus");
        bhapus.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bhapusActionPerformed(evt);
            }
        });

        bbatal.setText("Batal");
        bbatal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bbatalActionPerformed(evt);
            }
        });

        bcetak.setText("Cetak");
        bcetak.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bcetakActionPerformed(evt);
            }
        });
        bcetak.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                bcetakKeyPressed(evt);
            }
        });

        bkeluar.setText("Keluar");
        bkeluar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bkeluarActionPerformed(evt);
            }
        });

        jPanel7.setBackground(new java.awt.Color(255, 255, 255));
        jPanel7.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)), "Rincian Gaji"));
        jPanel7.setToolTipText("");
        jPanel7.setMaximumSize(new java.awt.Dimension(308, 196));
        jPanel7.setMinimumSize(new java.awt.Dimension(308, 196));

        jLabel22.setText("Nama Jabatan");

        jLabel23.setText("Gaji Pokok");

        txtnamajabatan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtnamajabatanActionPerformed(evt);
            }
        });

        jLabel27.setText("ID Jabatan");

        jLabel28.setText("Kehadiran");

        jLabel29.setText("Tidak Hadir");

        jLabel30.setText("Total Jam Lembur");

        txtjamlembur.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtjamlemburActionPerformed(evt);
            }
        });

        jLabel3.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel3.setText("Total Gaji");

        txttotalgaji.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txttotalgajiActionPerformed(evt);
            }
        });
        txttotalgaji.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txttotalgajiKeyPressed(evt);
            }
        });

        bcarij.setText("Cari");
        bcarij.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bcarijActionPerformed(evt);
            }
        });

        jLabel24.setText("Total Upah Lembur");

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel23)
                    .addComponent(jLabel22)
                    .addComponent(jLabel27)
                    .addComponent(jLabel28)
                    .addComponent(jLabel29)
                    .addComponent(jLabel30)
                    .addComponent(jLabel24)
                    .addComponent(jLabel3))
                .addGap(22, 22, 22)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel7Layout.createSequentialGroup()
                        .addComponent(txtidjabatan)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(bcarij))
                    .addGroup(jPanel7Layout.createSequentialGroup()
                        .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(txtgajipokok, javax.swing.GroupLayout.DEFAULT_SIZE, 166, Short.MAX_VALUE)
                            .addComponent(txttidakhadir, javax.swing.GroupLayout.DEFAULT_SIZE, 166, Short.MAX_VALUE)
                            .addComponent(txtkehadiran, javax.swing.GroupLayout.DEFAULT_SIZE, 166, Short.MAX_VALUE)
                            .addComponent(txtnamajabatan, javax.swing.GroupLayout.DEFAULT_SIZE, 166, Short.MAX_VALUE)
                            .addComponent(txtjamlembur)
                            .addComponent(txttotalupahlembur)
                            .addComponent(txttotalgaji))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );

        jPanel7Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {txtgajipokok, txtkehadiran, txtnamajabatan, txttidakhadir});

        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel27)
                    .addComponent(txtidjabatan, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bcarij))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel22)
                    .addComponent(txtnamajabatan, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel23)
                    .addComponent(txtgajipokok, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel28)
                    .addComponent(txtkehadiran, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel29)
                    .addComponent(txttidakhadir, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel30)
                    .addComponent(txtjamlembur, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel24)
                    .addComponent(txttotalupahlembur, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(txttotalgaji, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jLabel8.setText("ID Slip Gaji");

        jtgl.setModel(new javax.swing.SpinnerDateModel());

        jLabel9.setText("Tanggal");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(8, 8, 8)
                        .addComponent(jLabel8)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(txtidgaji, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel9)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jtgl, javax.swing.GroupLayout.PREFERRED_SIZE, 153, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jPanel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(bsimpan)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bhapus, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bbatal, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bcetak, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bkeluar, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8)
                    .addComponent(txtidgaji, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jtgl, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel9))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(bsimpan)
                        .addComponent(bhapus)
                        .addComponent(bbatal)
                        .addComponent(bcetak)
                        .addComponent(bkeluar))
                    .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void bsimpanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bsimpanActionPerformed
        format_jam();
        tambah();
    }//GEN-LAST:event_bsimpanActionPerformed

    private void bkeluarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bkeluarActionPerformed
        dispose();
    }//GEN-LAST:event_bkeluarActionPerformed

    private void bhapusActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bhapusActionPerformed
        int index = tblgaji.getSelectedRow();
        tabmode.removeRow(index);
        tblgaji.setModel(tabmode);
        kosong();
    }//GEN-LAST:event_bhapusActionPerformed

    private void bbatalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bbatalActionPerformed
        kosong();
        aktif();
        autonumber();
    }//GEN-LAST:event_bbatalActionPerformed

    private void tblgajiMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblgajiMouseClicked

    }//GEN-LAST:event_tblgajiMouseClicked

    private void txtidpegawaiActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtidpegawaiActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtidpegawaiActionPerformed

    private void txtnamajabatanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtnamajabatanActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtnamajabatanActionPerformed

    private void bcariaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bcariaActionPerformed
        popup_absensi Pa = new popup_absensi();
        Pa.abs = this;
        Pa.setVisible(true);
        Pa.setResizable(false);
    }//GEN-LAST:event_bcariaActionPerformed

    private void bcarijActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bcarijActionPerformed
        popup_jabatan2 Pj = new popup_jabatan2();
        Pj.jbtn2 = this;
        Pj.setVisible(true);
        Pj.setResizable(false);
    }//GEN-LAST:event_bcarijActionPerformed

    private void txtjamlemburActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtjamlemburActionPerformed

    }//GEN-LAST:event_txtjamlemburActionPerformed

    private void txttotalgajiActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txttotalgajiActionPerformed

    }//GEN-LAST:event_txttotalgajiActionPerformed

    private void txttotalgajiKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txttotalgajiKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            format_jam();
            tambah();
            bcetak.requestFocus();
        }
    }//GEN-LAST:event_txttotalgajiKeyPressed

    private void bcetakActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bcetakActionPerformed
        aktif();
        simpan();
    }//GEN-LAST:event_bcetakActionPerformed

    private void tblgajiKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tblgajiKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            aktif();
            simpan();
        }
    }//GEN-LAST:event_tblgajiKeyPressed

    private void bcetakKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_bcetakKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            simpan();
        }
    }//GEN-LAST:event_bcetakKeyPressed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(hitung_gaji.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(hitung_gaji.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(hitung_gaji.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(hitung_gaji.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new hitung_gaji().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton bbatal;
    private javax.swing.JButton bcaria;
    private javax.swing.JButton bcarij;
    private javax.swing.JButton bcetak;
    private javax.swing.JButton bhapus;
    private javax.swing.JButton bkeluar;
    private javax.swing.JButton bsimpan;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel27;
    private javax.swing.JLabel jLabel28;
    private javax.swing.JLabel jLabel29;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel30;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JSpinner jtgl;
    private com.toedter.calendar.JDateChooser kalender;
    private javax.swing.JRadioButton rlaki;
    private javax.swing.JRadioButton rperempuan;
    private javax.swing.JTable tblgaji;
    private javax.swing.JTextField txtgajipokok;
    private javax.swing.JTextField txtidabsensi;
    private javax.swing.JTextField txtidgaji;
    private javax.swing.JTextField txtidjabatan;
    private javax.swing.JTextField txtidpegawai;
    private javax.swing.JTextField txtjabatan;
    private javax.swing.JTextField txtjamlembur;
    private javax.swing.JTextField txtkehadiran;
    private javax.swing.JTextField txtnama;
    private javax.swing.JTextField txtnamajabatan;
    private javax.swing.JTextField txtnotelp;
    private javax.swing.JTextField txttidakhadir;
    private javax.swing.JTextField txttotalgaji;
    private javax.swing.JTextField txttotalupahlembur;
    // End of variables declaration//GEN-END:variables
}
