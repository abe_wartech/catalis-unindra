-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 28, 2020 at 03:41 AM
-- Server version: 5.5.39
-- PHP Version: 5.4.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `sistem_penggajian`
--

-- --------------------------------------------------------

--
-- Table structure for table `absensi`
--

CREATE TABLE IF NOT EXISTS `absensi` (
  `bulan` varchar(20) NOT NULL,
  `id_absensi` varchar(15) NOT NULL,
  `id_pegawai` varchar(15) NOT NULL,
  `nama_pegawai` varchar(30) NOT NULL,
  `jenis_kelamin` varchar(15) NOT NULL,
  `notelp` varchar(15) NOT NULL,
  `jabatan` varchar(30) NOT NULL,
  `kehadiran` int(20) NOT NULL,
  `tidak_hadir` int(20) NOT NULL,
  `total_jam_lembur` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `absensi`
--

INSERT INTO `absensi` (`bulan`, `id_absensi`, `id_pegawai`, `nama_pegawai`, `jenis_kelamin`, `notelp`, `jabatan`, `kehadiran`, `tidak_hadir`, `total_jam_lembur`) VALUES
('Februari 2020', 'IA0001', 'IP0001', 'Muhammad Rizky Yanwar', 'Laki-Laki', '089999888777', 'Junior Consultant', 20, 0, 8),
('Februari 2020', 'IA0002', 'IP0002', 'Muhammad Farhat Djehan Kholiq', 'Laki-Laki', '081213141516', 'Client Services Officer', 20, 0, 8),
('Februari 2020', 'IA0003', 'IP0003', 'Ramad Al Habib Khasary', 'Laki-Laki', '086655554444', 'Programmer', 20, 0, 9),
('Februari 2020', 'IA0004', 'IP0004', 'Rico Aviano', 'Laki-Laki', '088811113333', 'Administrator', 18, 2, 7),
('Februari 2020', 'IA0005', 'IP0005', 'Muthmainah', 'Perempuan', '087788889999', 'Administrator', 17, 3, 5),
('Februari 2020', 'IA0006', 'IP0006', 'Maulana Malik Muhammad', 'Laki-Laki', '081122223333', 'Tracker', 19, 1, 6),
('Februari 2020', 'IA0007', 'IP0007', 'Muhammad Adi Pradana', 'Laki-Laki', '087755552222', 'Tracker', 18, 2, 4),
('Februari 2020', 'IA0008', 'IP0008', 'Sugeng Prayetno', 'Laki-Laki', '088822226666', 'Tracker', 18, 2, 7);

-- --------------------------------------------------------

--
-- Table structure for table `jabatan`
--

CREATE TABLE IF NOT EXISTS `jabatan` (
  `id_jabatan` varchar(10) NOT NULL,
  `nama_jabatan` varchar(30) NOT NULL,
  `gaji_pokok` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `jabatan`
--

INSERT INTO `jabatan` (`id_jabatan`, `nama_jabatan`, `gaji_pokok`) VALUES
('IJ0001', 'Client Services Officer', 5000000),
('IJ0002', 'Junior Consultant', 4500000),
('IJ0003', 'Administrator', 4000000),
('IJ0004', 'Programmer', 4500000),
('IJ0005', 'Tracker', 3500000),
('IJ0006', 'Office Boy', 3000000);

-- --------------------------------------------------------

--
-- Table structure for table `pegawai`
--

CREATE TABLE IF NOT EXISTS `pegawai` (
  `id_pegawai` varchar(10) NOT NULL,
  `nama_pegawai` varchar(30) NOT NULL,
  `jenis_kelamin` varchar(15) NOT NULL,
  `tgl_lahir` varchar(20) NOT NULL,
  `notelp` varchar(15) NOT NULL,
  `jabatan` varchar(30) NOT NULL,
  `alamat` varchar(300) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pegawai`
--

INSERT INTO `pegawai` (`id_pegawai`, `nama_pegawai`, `jenis_kelamin`, `tgl_lahir`, `notelp`, `jabatan`, `alamat`) VALUES
('IP0001', 'Muhammad Rizky Yanwar', 'Laki-Laki', '1998-05-19', '089999888777', 'Junior Consultant', 'JL. Merah Putih, Depok'),
('IP0002', 'Muhammad Farhat Djehan Kholiq', 'Laki-Laki', '1998-04-08', '081213141516', 'Client Services Officer', 'Grand Depok City, Ruko Anggrek 1 No. 18'),
('IP0003', 'Ramad Al Habib Khasary', 'Laki-Laki', '1998-04-07', '086655554444', 'Programmer', 'JL Cibinong, Bogor'),
('IP0004', 'Rico Aviano', 'Laki-Laki', '1998-08-20', '088811113333', 'Administrator', 'JL. Kelapa Dua, Depok'),
('IP0005', 'Muthmainah', 'Perempuan', '1998-07-19', '087788889999', 'Administrator', 'JL. Bojong, Depok'),
('IP0006', 'Maulana Malik Muhammad', 'Laki-Laki', '1996-03-10', '081122223333', 'Tracker', 'JL. Matraman, Jakarta Selatan'),
('IP0007', 'Muhammad Adi Pradana', 'Laki-Laki', '1996-11-25', '087755552222', 'Tracker', 'JL. Arundina, Jakarta Timur'),
('IP0008', 'Sugeng Prayetno', 'Laki-Laki', '1998-12-23', '088822226666', 'Tracker', 'JL. Bumiayu, Bogor');

-- --------------------------------------------------------

--
-- Table structure for table `slip_gaji`
--

CREATE TABLE IF NOT EXISTS `slip_gaji` (
  `tanggal` varchar(25) NOT NULL,
  `id_slip_gaji` varchar(15) NOT NULL,
  `bulan` varchar(20) NOT NULL,
  `id_absensi` varchar(15) NOT NULL,
  `id_pegawai` varchar(15) NOT NULL,
  `nama_pegawai` varchar(50) NOT NULL,
  `id_jabatan` varchar(15) NOT NULL,
  `nama_jabatan` varchar(50) NOT NULL,
  `gaji_pokok` varchar(50) NOT NULL,
  `kehadiran` varchar(10) NOT NULL,
  `tidak_hadir` varchar(10) NOT NULL,
  `total_jam_lembur` varchar(10) NOT NULL,
  `total_upah_lembur` varchar(50) NOT NULL,
  `total_gaji` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `slip_gaji`
--

INSERT INTO `slip_gaji` (`tanggal`, `id_slip_gaji`, `bulan`, `id_absensi`, `id_pegawai`, `nama_pegawai`, `id_jabatan`, `nama_jabatan`, `gaji_pokok`, `kehadiran`, `tidak_hadir`, `total_jam_lembur`, `total_upah_lembur`, `total_gaji`) VALUES
('2020-02-28, 09:24:45', 'IG0001', 'Februari 2020', 'IA0001', 'IP0001', 'Muhammad Rizky Yanwar', 'IJ0002', 'Junior Consultant', 'Rp. 4.500.000,00', '20 Hari', '0 Hari', '8 Jam', 'Rp. 403.170,00', 'Rp. 4.903.170,00'),
('2020-02-28, 09:25:02', 'IG0002', 'Februari 2020', 'IA0002', 'IP0002', 'Muhammad Farhat Djehan Kholiq', 'IJ0001', 'Client Services Officer', 'Rp. 5.000.000,00', '20 Hari', '0 Hari', '8 Jam', 'Rp. 447.965,00', 'Rp. 5.447.965,00'),
('2020-02-28, 09:25:15', 'IG0003', 'Februari 2020', 'IA0003', 'IP0003', 'Ramad Al Habib Khasary', 'IJ0004', 'Programmer', 'Rp. 4.500.000,00', '20 Hari', '0 Hari', '9 Jam', 'Rp. 455.192,00', 'Rp. 4.955.192,00'),
('2020-02-28, 09:25:26', 'IG0004', 'Februari 2020', 'IA0004', 'IP0004', 'Rico Aviano', 'IJ0003', 'Administrator', 'Rp. 4.000.000,00', '18 Hari', '2 Hari', '7 Jam', 'Rp. 312.133,00', 'Rp. 4.312.133,00'),
('2020-02-28, 09:25:40', 'IG0005', 'Februari 2020', 'IA0005', 'IP0005', 'Muthmainah', 'IJ0003', 'Administrator', 'Rp. 4.000.000,00', '17 Hari', '3 Hari', '5 Jam', 'Rp. 219.649,00', 'Rp. 4.219.649,00'),
('2020-02-28, 09:25:50', 'IG0006', 'Februari 2020', 'IA0006', 'IP0006', 'Maulana Malik Muhammad', 'IJ0005', 'Tracker', 'Rp. 3.500.000,00', '19 Hari', '1 Hari', '6 Jam', 'Rp. 232.656,00', 'Rp. 3.732.656,00'),
('2020-02-28, 09:25:58', 'IG0007', 'Februari 2020', 'IA0007', 'IP0007', 'Muhammad Adi Pradana', 'IJ0005', 'Tracker', 'Rp. 3.500.000,00', '18 Hari', '2 Hari', '4 Jam', 'Rp. 151.732,00', 'Rp. 3.651.732,00'),
('2020-02-28, 09:26:07', 'IG0008', 'Februari 2020', 'IA0008', 'IP0008', 'Sugeng Prayetno', 'IJ0005', 'Tracker', 'Rp. 3.500.000,00', '18 Hari', '2 Hari', '7 Jam', 'Rp. 273.118,00', 'Rp. 3.773.118,00');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `nama_pegawai` varchar(50) NOT NULL,
  `jabatan` varchar(50) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`nama_pegawai`, `jabatan`, `username`, `password`) VALUES
('admin', 'Administrator', 'admin', 'admin');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `absensi`
--
ALTER TABLE `absensi`
 ADD PRIMARY KEY (`id_absensi`);

--
-- Indexes for table `jabatan`
--
ALTER TABLE `jabatan`
 ADD PRIMARY KEY (`id_jabatan`);

--
-- Indexes for table `pegawai`
--
ALTER TABLE `pegawai`
 ADD PRIMARY KEY (`id_pegawai`);

--
-- Indexes for table `slip_gaji`
--
ALTER TABLE `slip_gaji`
 ADD PRIMARY KEY (`id_slip_gaji`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
 ADD PRIMARY KEY (`jabatan`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
