-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 19 Jun 2019 pada 18.59
-- Versi server: 10.1.38-MariaDB
-- Versi PHP: 7.3.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `gajipegawai`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbldivisi`
--

CREATE TABLE `tbldivisi` (
  `id_divisi` varchar(20) NOT NULL,
  `nama_divisi` varchar(30) NOT NULL,
  `keterangan` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbldivisi`
--

INSERT INTO `tbldivisi` (`id_divisi`, `nama_divisi`, `keterangan`) VALUES
('1219001', 'Keuangan', 'Untuk mengatur keuangan dalam perusahaan agar bisa mendapatkan laba yang diinginkan.'),
('1230301', 'Operasional', 'Mengelola dan mengarahkan tim operasi untuk mencapai target bisnis, Membantu untuk mengembangkan atau memperbarui prosedur operasi standar untuk semua kegiatan operasional bisnis.'),
('1267801', 'Produksi', 'Melaksanakan administrasi Bidang Produksi, Melaksanakan penyusunan program dan rencana kerja Bidang Produksi, Melaksanakan koordinasi dengan unit kerja lain.'),
('1284001', 'Personalia', 'Penerimaan tenaga kerja koordinasi dengan labour supply, Sosialisasi dan koordinasi, Menyiapkan perjanjian kerja dengan karyawan baru.'),
('1299701', 'Pemasaran', 'Melakukan perencanaan strategi pemasaran dengan memperhatikan trend pasar dan sumber daya perusahaan, Merencanakan marketing research yaitu dengan mengikuti perkembangan pasar terutama terhadap produk yang sejenis dari perusahaan pesaing.');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tblgaji`
--

CREATE TABLE `tblgaji` (
  `id_slipgaji` varchar(25) NOT NULL,
  `id_pegawai` varchar(30) NOT NULL,
  `id_jabatan` varchar(25) NOT NULL,
  `gaji_pokok` int(30) NOT NULL,
  `kehadiran` varchar(10) NOT NULL,
  `tunjangan` int(30) NOT NULL,
  `bonus` int(30) NOT NULL,
  `lembur` int(30) NOT NULL,
  `total_gaji` int(30) NOT NULL,
  `tgl` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tblgaji`
--

INSERT INTO `tblgaji` (`id_slipgaji`, `id_pegawai`, `id_jabatan`, `gaji_pokok`, `kehadiran`, `tunjangan`, `bonus`, `lembur`, `total_gaji`, `tgl`) VALUES
('DG0001', '0805001', '1010303', 30000000, '24', 10000000, 10000000, 0, 50000000, '2019-06-19'),
('DG0002', '0805002', '1010301', 30000000, '25', 10000000, 9000000, 0, 49000000, '2019-06-19');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbljabatan`
--

CREATE TABLE `tbljabatan` (
  `id_jabatan` varchar(20) NOT NULL,
  `id_divisi` varchar(20) NOT NULL,
  `nama_jabatan` varchar(30) NOT NULL,
  `gaji_pokok` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbljabatan`
--

INSERT INTO `tbljabatan` (`id_jabatan`, `id_divisi`, `nama_jabatan`, `gaji_pokok`) VALUES
('1010101', '1219001', 'Supervisor', '13000000'),
('1010102', '1284001', 'Supervisor', '13000000'),
('1010103', '1267801', 'Supervisor', '13000000'),
('1010104', '1230301', 'Supervisor', '13000000'),
('1010105', '1299701', 'Supervisor', '13000000'),
('1010201', '1284001', 'Staff', '5000000'),
('1010202', '1219001', 'Staff', '5000000'),
('1010203', '1267801', 'Staff', '5000000'),
('1010204', '1230301', 'Staff', '5000000'),
('1010205', '1299701', 'Staff', '5000000'),
('1010301', '1284001', 'Direktur', '30000000'),
('1010302', '1267801', 'Direktur', '30000000'),
('1010303', '1219001', 'Direktur', '30000000'),
('1010304', '1230301', 'Direktur', '30000000'),
('1010305', '1299701', 'Direktur', '30000000'),
('1010401', '1284001', 'Office Boy', '2500000'),
('1010402', '1267801', 'Office Boy', '2500000'),
('1010403', '1230301', 'Office Boy', '2500000'),
('1010404', '1219001', 'Office Boy', '2500000'),
('1010405', '1299701', 'Office Boy', '2500000'),
('1010501', '1284001', 'Wakil Direktur', '25000000'),
('1010502', '1267801', 'Wakil Direktur', '25000000'),
('1010503', '1230301', 'Wakil Direktur', '25000000'),
('1010504', '1299701', 'Wakil Direktur', '25000000'),
('1010505', '1219001', 'Wakil Direktur', '25000000'),
('1012501', '1284001', 'Manager', '17000000'),
('1012502', '1267801', 'Manager', '17000000'),
('1012503', '1230301', 'Manager', '17000000'),
('1012504', '1299701', 'Manager', '17000000'),
('1012505', '1219001', 'Manager', '17000000'),
('1013010', '1284001', 'Sekretaris', '9000000'),
('1013011', '1267801', 'Sekretaris', '9000000'),
('1013012', '1219001', 'Sekretaris', '9000000'),
('1013013', '1230301', 'Sekretaris', '9000000'),
('1013014', '1299701', 'Sekretaris', '9000000');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tblpegawai`
--

CREATE TABLE `tblpegawai` (
  `id_pegawai` varchar(20) NOT NULL,
  `id_jabatan` varchar(20) NOT NULL,
  `nama` varchar(30) NOT NULL,
  `jenkel` varchar(10) NOT NULL,
  `alamat` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tblpegawai`
--

INSERT INTO `tblpegawai` (`id_pegawai`, `id_jabatan`, `nama`, `jenkel`, `alamat`) VALUES
('0805001', '1010303', 'Malik', 'laki-laki', 'Tebet , Jakarta-Selatan'),
('0805002', '1010301', 'Alisya Calista', 'perempuan', 'Kelapa Gading , Jakarta-Utara'),
('0805003', '1010302', 'Angelina Augustia', 'perempuan', 'Menteng , Jakarta-Pusat'),
('0805004', '1010304', 'Alvaro Quilan', 'laki-laki', 'Blok-M , Jakarta-Selatan'),
('0805005', '1010305', 'Rini Aulia', 'perempuan', 'Tebet , Jakarta-Selatan'),
('0807001', '1010501', 'Ajeng Puspita', 'perempuan', 'Depok , Jawa Barat'),
('0807002', '1010502', 'Putri Damayanti', 'perempuan', 'Mampang , Jakarta-Selatan'),
('0807003', '1010503', 'Kurniawan ', 'laki-laki', 'Bogor , Jawa Barat'),
('0807004', '1010504', 'Hadi', 'laki-laki', 'Cileungsi , Jawa Barat'),
('0807005', '1010505', 'Febrilio', 'laki-laki', 'Jonggol , Jawa Barat'),
('0807006', '1012501', 'Ahmad Mauludy', 'laki-laki', 'Cilandak , Jakarta-Selatan'),
('0807007', '1012502', 'Adi Pradana', 'laki-laki', 'Ciracas , Jakarta-Timur'),
('0807008', '1012503', 'Reza Pratama', 'laki-laki', 'Condet , Jakarta-Timur'),
('0807009', '1012504', 'Carissa Nasution', 'perempuan', 'Bangka , Jakarta-Selatan'),
('0807010', '1012505', 'Suci Salsabilah', 'perempuan', 'Tebet , Jakarta-Selatan'),
('0807011', '1010101', 'Ahmad Setyo', 'laki-laki', 'Kp Melayu , Jakarta-Timur'),
('0807012', '1010102', 'Fauziah Ramadhania', 'perempuan', 'Pisangan Lama , Jakarta-Timur'),
('0807013', '1010103', 'Fauzan Utama', 'laki-laki', 'Otista , Jakarta-Timur'),
('0807014', '1010104', 'Clara Saputri', 'perempuan', 'Blok-M , Jakarta-Selatan'),
('0807015', '1010105', 'Hilda Rahayu', 'perempuan', 'Menteng Dalam , Jakarta-Selatan'),
('0810001', '1013010', 'Agatha Oktaviani', 'perempuan', 'Menteng Dalam , Jakarta-Selatan'),
('0810002', '1013011', 'Yulianti Fadhilah', 'perempuan', 'Menteng Wadas , Jakarta-Selatan'),
('0810003', '1013012', 'Meri Wijaya', 'perempuan', 'Setia Budi , Jakarta-Selatan'),
('0810004', '1013013', 'Alya Dwi Aryanti', 'perempuan', 'Kemang , Jakarta-Selatan'),
('0810005', '1013014', 'Feby Anggraini Putri', 'perempuan', 'Kemang , Jakarta-Selatan'),
('1001001', '1010201', 'Asya', 'perempuan', 'Pengadegan , Jakarta-Selata'),
('1001004', '1010201', 'Mustika', 'perempuan', 'Manggarai , Jakarta-Selatan'),
('1001005', '1010201', 'Dhea', 'perempuan', 'Kayu Manis , Jakarta-Timur10'),
('1001006', '1010201', 'Fadhil Herman', 'laki-laki', 'Jagakarsa , Jakarta-Selatan'),
('1002012', '1010203', 'Lala Lidya', 'perempuan', 'Bangka , Jakarta-Selatan'),
('1003009', '1010201', 'Putri Lestari', 'perempuan', 'Pondok Bambu , Jakarta-Timur'),
('1003014', '1010202', 'Ardian Septian', 'laki-laki', 'Pengadegan , Jakarta-Selatan'),
('1004002', '1010201', 'Nabilah Fahira', 'perempuan', 'Kelapa Gading , Jakarta-Utara'),
('1005004', '1010201', 'Akbar Ramadhan', 'laki-laki', 'Pondok Gede , Jakarta-Timur'),
('1005005', '1010203', 'Novi Yanna', 'perempuan', 'Cipinang Besar , Jakarta-Timur'),
('1005011', '1010202', 'Alfian Hermawan', 'laki-laki', 'Manggarai , Jakarta-Selatan'),
('1005013', '1010202', 'Dinda Agustin', 'perempuan', 'Duren Tiga, Jakarta-Selatan'),
('1005017', '1010204', 'Ryan Firdaus', 'laki-laki', 'Pasar Minggu , Jakarta-Selatan'),
('1006003', '1010202', 'Dini Aulia', 'perempuan', 'Kalibata , Jakarta-Selatan'),
('1006009', '1010204', 'Afri Dewita', 'perempuan', 'Pejaten , Jakarta-Selatan'),
('1007018', '1010203', 'Wahyudi Rizal', 'laki-laki', 'Cawang , Jakarta-Timur'),
('1101007', '1010202', 'Rizky Saputra', 'laki-laki', 'Setia Budi , Jakarta-Selatan'),
('1102002', '1010204', 'Lisa Afriani', 'perempuan', 'Pejaten , Jakarta-Selatan'),
('1103003', '1010201', 'Ahmad Setia', 'laki-laki', 'Karet , Jakarta-Selatan'),
('1103005', '1010204', '', 'perempuan', 'Palmerah , Jakarta-Selatan'),
('1103008', '1010203', 'Ricky Syahbani', 'laki-laki', 'Kp Pulo , Jakarta-Timur'),
('1104004', '1010203', 'Rizka Ameliani', 'perempuan', 'Kuningan , Jakarta-Selatan'),
('1104011', '1010202', 'Nita Safira', 'perempuan', 'Duren Tiga , Jakarta-Selatan'),
('1106017', '1010202', 'Ahmad Shobari', 'laki-laki', 'Kebon Baru , Jakarta-Selatan'),
('1203002', '1010202', 'Tiara Julia', 'perempuan', 'Veteran , Jakarta-Selatan'),
('1203008', '1010203', 'Anne Rizky', 'perempuan', 'Kemang , Jakarta-Selatan'),
('1204008', '1010204', 'Robby Alkausar', 'laki-laki', 'Blok M , Jakarta-Selatan'),
('1206014', '1010204', 'Adelia Oktaviani', 'perempuan', 'Menteng Dalam , Jakarta-Selatan'),
('1304005', '1010203', 'Maulidya', 'perempuan', 'Kemang , Jakarta-Selatan'),
('1304015', '1010204', 'Luthfi Syarif Pratama', 'laki-laki', 'Pondok Bambu , Jakarta-Timur'),
('1305009', '1010203', 'Zakaria Hafizh', 'laki-laki', 'Pancoran , Jakarta-Selatan'),
('1309003', '1010204', 'Yogi Chandra', 'laki-laki', 'Bendungan Hilir , Jakarta-Pusat'),
('1604009', '1010401', 'Yuni Audia', 'perempuan', 'Manggarai , Jakarta-Selatan'),
('1605005', '1010402', 'Nugroho Seto Pamungkas', 'laki-laki', 'Setia Budi , Jakarta-Selatan'),
('1606015', '1010405', 'Nadya Ayu Febrilia', 'perempuan', 'Bukit Duri , Jakarta-Selatan'),
('1608003', '1010402', 'Bayu Permana', 'laki-laki', 'Bukit Duri , Jakarta-Selatan'),
('1702005', '1010405', 'Endang Pradana', 'laki-laki', 'Menteng Atas , Jakarta-Selatan'),
('1703001', '1010403', 'Ahmad Ghafary', 'laki-laki', 'Tomang , Jakarta-Barat'),
('1703009', '1010404', 'Erlangga Hertanto', 'laki-laki', 'Percetakan Negara , Jakarta-Pusat'),
('1704008', '1010404', 'Gina Putri Cempaka', 'perempuan', 'Karet , Jakarta-Selatan'),
('1705007', '1010403', 'Aldy Pamungkas', 'laki-laki', 'Bukit Duri , Jakarta-Selatan'),
('1705010', '1010404', 'Dinda Tasya Rahayu', 'perempuan', 'Kayu Manis , Jakarta-Timur'),
('1707003', '1010403', 'Ayu Ningtyas', 'perempuan', 'Utan Kayu , Jakarta-Timur'),
('1709001', '1010401', 'Joko Santoso', 'laki-laki', 'Kp.Melayu , Jakarta-Timur'),
('1802008', '1010403', 'Muhammad Ridho Aditya', 'laki-laki', 'Cipinang Muara , Jakarta-Timur'),
('1802011', '1010402', 'Rizky Daud Saputra', 'laki-laki', 'Cipinang Muara , Jakarta-Timur'),
('1804010', '1010401', 'Andi Permana', 'laki-laki', 'Percetakan Negara , Jakarta-Pusat'),
('1804018', '1010401', 'Pratama Putra', 'laki-laki', 'Bukit Duri , Jakarta-Selatan'),
('1805004', '1010405', 'Inem', 'perempuan', 'Kwitang , Jakarta-Pusat'),
('1805007', '1010404', 'Siti Zubaidah', 'perempuan', 'Cipinang Besar , Jakarta-Timur'),
('1809002', '1010403', 'Aditya Suherman', 'laki-laki', 'Tanah Tinggi , Jakarta-Pusat'),
('1809015', '1010401', 'Kokom Zakiah', 'perempuan', 'Paseban , Jakarta-Pusat'),
('1902004', '1010402', 'Jihan Wandayani', 'perempuan', 'Kramat Sawah , Jakarta-Pusat');

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE `user` (
  `Username` varchar(30) NOT NULL,
  `Password` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`Username`, `Password`) VALUES
('Malik', 'maret');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `tbldivisi`
--
ALTER TABLE `tbldivisi`
  ADD PRIMARY KEY (`id_divisi`);

--
-- Indeks untuk tabel `tblgaji`
--
ALTER TABLE `tblgaji`
  ADD PRIMARY KEY (`id_slipgaji`);

--
-- Indeks untuk tabel `tbljabatan`
--
ALTER TABLE `tbljabatan`
  ADD PRIMARY KEY (`id_jabatan`);

--
-- Indeks untuk tabel `tblpegawai`
--
ALTER TABLE `tblpegawai`
  ADD PRIMARY KEY (`id_pegawai`);

--
-- Indeks untuk tabel `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`Username`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
