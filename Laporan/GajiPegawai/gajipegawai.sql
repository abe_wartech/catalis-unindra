-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 03, 2019 at 02:47 PM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `gajipegawai`
--

-- --------------------------------------------------------

--
-- Table structure for table `tblabsensi`
--

CREATE TABLE `tblabsensi` (
  `id_pegawai` varchar(50) NOT NULL,
  `nm_pegawai` varchar(50) NOT NULL,
  `nm_divisi` varchar(50) NOT NULL,
  `nm_jabatan` varchar(50) NOT NULL,
  `alfa` varchar(50) NOT NULL,
  `kehadiran` varchar(50) NOT NULL,
  `tgl` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblabsensi`
--

INSERT INTO `tblabsensi` (`id_pegawai`, `nm_pegawai`, `nm_divisi`, `nm_jabatan`, `alfa`, `kehadiran`, `tgl`) VALUES
('0805001', 'Maulana Malik', 'Keuangan', 'Direktur', '0', '18', '2019-12-03'),
('0805002', 'Alisya Calista', 'Personalia', 'Direktur', '0', '18', '2019-12-03'),
('0805003', 'Angelina Augustia', 'Produksi', 'Direktur', '0', '18', '2019-12-03'),
('0805004', 'Alvaro Quilan', 'Operasional', 'Direktur', '0', '18', '2019-12-03'),
('0805005', 'Rini Aulia', 'Pemasaran', 'Direktur', '0', '18', '2019-12-03');

-- --------------------------------------------------------

--
-- Table structure for table `tbldivisi`
--

CREATE TABLE `tbldivisi` (
  `id_divisi` varchar(20) NOT NULL,
  `nama_divisi` varchar(30) NOT NULL,
  `keterangan` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbldivisi`
--

INSERT INTO `tbldivisi` (`id_divisi`, `nama_divisi`, `keterangan`) VALUES
('1219001', 'Keuangan', 'Untuk mengatur keuangan dalam perusahaan agar bisa mendapatkan laba yang diinginkan.'),
('1230301', 'Operasional', 'Mengelola dan mengarahkan tim operasi untuk mencapai target bisnis, Membantu untuk mengembangkan atau memperbarui prosedur operasi standar untuk semua kegiatan operasional bisnis.'),
('1267801', 'Produksi', 'Melaksanakan administrasi Bidang Produksi, Melaksanakan penyusunan program dan rencana kerja Bidang Produksi, Melaksanakan koordinasi dengan unit kerja lain.'),
('1284001', 'Personalia', 'Penerimaan tenaga kerja koordinasi dengan labour supply, Sosialisasi dan koordinasi, Menyiapkan perjanjian kerja dengan karyawan baru.'),
('1299701', 'Pemasaran', 'Melakukan perencanaan strategi pemasaran dengan memperhatikan trend pasar dan sumber daya perusahaan, Merencanakan marketing research yaitu dengan mengikuti perkembangan pasar terutama terhadap produk yang sejenis dari perusahaan pesaing.');

-- --------------------------------------------------------

--
-- Table structure for table `tblgaji`
--

CREATE TABLE `tblgaji` (
  `id_slipgaji` varchar(25) NOT NULL,
  `id_pegawai` varchar(30) NOT NULL,
  `id_jabatan` varchar(25) NOT NULL,
  `id_divisi` varchar(20) NOT NULL,
  `nama_pegawai` varchar(50) NOT NULL,
  `gaji_pokok` int(30) NOT NULL,
  `kehadiran` varchar(10) NOT NULL,
  `tunjangan` int(30) NOT NULL,
  `bonus` int(30) NOT NULL,
  `lembur` int(30) NOT NULL,
  `total_gaji` int(30) NOT NULL,
  `tgl` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblgaji`
--

INSERT INTO `tblgaji` (`id_slipgaji`, `id_pegawai`, `id_jabatan`, `id_divisi`, `nama_pegawai`, `gaji_pokok`, `kehadiran`, `tunjangan`, `bonus`, `lembur`, `total_gaji`, `tgl`) VALUES
('DG0001', '0805001', '1010303', '1219001', 'Maulana Malik', 28000000, '18', 7000000, 15000000, 0, 50000000, '2019-06-19'),
('DG0002', '0805002', '1010301', '1284001', 'Alisya Calista', 28000000, '18', 7000000, 13000000, 0, 48000000, '2019-06-19'),
('DG0003', '0805003', '1010302', '1267801', 'Angelina Augustia', 28000000, '18', 7000000, 10000000, 0, 45000000, '2019-06-21'),
('DG0004', '0805004', '1010304', '1230301', 'Alvaro Quilan', 28000000, '18', 7000000, 9000000, 0, 44000000, '2019-06-21'),
('DG0005', '0805005', '1010305', '1299701', 'Rini Aulia', 28000000, '18', 7000000, 8000000, 0, 43000000, '2019-06-21'),
('DG0006', '0807001', '1010501', '1219001', 'Ajeng Puspita', 23000000, '20', 7000000, 10000000, 0, 40000000, '2019-06-21'),
('DG0007', '0807002', '1010502', '1284001', 'Putri Damayanti', 23000000, '20', 7000000, 10000000, 0, 40000000, '2019-06-21'),
('DG0008', '0807003', '1010503', '1267801', 'Kurniawan', 23000000, '20', 7000000, 8000000, 0, 38000000, '2019-06-21'),
('DG0009', '0807004', '1010504', '1230301', 'Hadi', 23000000, '20', 7000000, 7000000, 0, 37000000, '2019-06-21'),
('DG0010', '0807005', '1010505', '1299701', 'Febrilio', 23000000, '20', 7000000, 9000000, 0, 39000000, '2019-06-21'),
('DG0011', '0807006', '1012501', '1230301', 'Ahmad Mauludy', 13000000, '22', 5000000, 7000000, 0, 25000000, '2019-06-21'),
('DG0012', '0807007', '1012502', '1284001', 'Fandi Bastian', 13000000, '22', 5000000, 7000000, 0, 25000000, '2019-06-21'),
('DG0013', '0807008', '1012503', '1267801', 'Reza Pratama', 13000000, '22', 5000000, 5000000, 0, 23000000, '2019-06-21'),
('DG0014', '0807009', '1012504', '1219001', 'Carissa Nasution', 13000000, '22', 5000000, 8000000, 0, 26000000, '2019-06-21'),
('DG0015', '0807010', '1012505', '1299701', 'Suci Salsabilah', 13000000, '22', 5000000, 8000000, 0, 26000000, '2019-06-21'),
('DG0016', '0807011', '1010101', '1284001', 'Ahmad Setyo', 10000000, '22', 4000000, 4000000, 0, 18000000, '2019-06-21'),
('DG0017', '0807012', '1010102', '1267801', 'Fauziah Ramadhania', 10000000, '22', 4000000, 5000000, 0, 19000000, '2019-06-21'),
('DG0018', '0807013', '1010103', '1230301', 'Fauzan Utama', 10000000, '22', 4000000, 4000000, 0, 18000000, '2019-06-21'),
('DG0019', '0807014', '1010104', '1219001', 'Clara Saputri', 10000000, '22', 4000000, 5000000, 0, 19000000, '2019-06-21'),
('DG0020', '0807015', '1010105', '1299701', 'Hilda Rahayu', 10000000, '22', 4000000, 5000000, 0, 19000000, '2019-06-21'),
('DG0021', '0810001', '1013010', '1219001', 'Agatha Oktaviani', 8000000, '22', 3000000, 4000000, 0, 15000000, '2019-06-21'),
('DG0022', '0810002', '1013011', '1284001', 'Yulianti Fadhilah', 8000000, '22', 3000000, 4000000, 0, 15000000, '2019-06-21'),
('DG0023', '0810003', '1013012', '1267801', 'Meri Wijaya', 8000000, '22', 3000000, 4000000, 0, 15000000, '2019-06-21'),
('DG0024', '0810004', '1013013', '1230301', 'Alya Dwi Aryanti', 8000000, '22', 3000000, 4000000, 0, 15000000, '2019-06-21'),
('DG0025', '0810005', '1013014', '1299701', 'Feby Anggraini Putri', 8000000, '22', 3000000, 4000000, 0, 15000000, '2019-06-21'),
('DG0026', '1604009', '1010401', '1219001', 'Yuni Audia', 2500000, '22', 1000000, 0, 0, 3500000, '2019-06-22'),
('DG0027', '1709001', '1010401', '1219001', 'Joko Santoso', 2500000, '22', 1000000, 0, 0, 3500000, '2019-06-22'),
('DG0028', '1804010', '1010401', '1219001', 'Andi Permana', 2500000, '22', 1000000, 0, 0, 3500000, '2019-06-22'),
('DG0029', '1804018', '1010401', '1219001', 'Pratama Putra', 2500000, '22', 1000000, 0, 0, 3500000, '2019-06-22'),
('DG0030', '1809015', '1010401', '1219001', 'Kokom Zakiah', 2500000, '22', 1000000, 0, 0, 3500000, '2019-06-22'),
('DG0031', '1605005', '1010402', '1284001', 'Nugroho Seto Pamungkas', 2500000, '22', 1000000, 0, 0, 3500000, '2019-06-22'),
('DG0032', '1608003', '1010402', '1284001', 'Bayu Permana', 2500000, '22', 1000000, 0, 0, 3500000, '2019-06-22'),
('DG0033', '1802011', '1010402', '1284001', 'Rizky Daud Saputra', 2500000, '22', 1000000, 0, 0, 3500000, '2019-06-22'),
('DG0034', '1902004', '1010402', '1284001', 'Jihan Wandayani', 2500000, '22', 1000000, 0, 0, 3500000, '2019-06-22'),
('DG0035', '1703001', '1010403', '1267801', 'Ahmad Ghafary', 2500000, '22', 1000000, 0, 0, 3500000, '2019-06-22'),
('DG0036', '1705007', '1010403', '1267801', 'Aldy Pamungkas', 2500000, '22', 1000000, 0, 0, 3500000, '2019-06-22'),
('DG0037', '1707003', '1010403', '1267801', 'Ayu Ningtyas', 2500000, '22', 1000000, 0, 0, 3500000, '2019-06-22'),
('DG0038', '1802008', '1010403', '1267801', 'Muhammad Ridho Aditya', 2500000, '22', 1000000, 0, 0, 3500000, '2019-06-22'),
('DG0039', '1809002', '1010403', '1267801', 'Aditya Suherman', 2500000, '22', 1000000, 0, 0, 3500000, '2019-06-22'),
('DG0040', '1703009', '1010404', '1230301', 'Erlangga Hertanto', 2500000, '22', 1000000, 0, 0, 3500000, '2019-06-22'),
('DG0041', '1704008', '1010404', '1230301', 'Gina Putri Cempaka', 2500000, '22', 1000000, 0, 0, 3500000, '2019-06-22'),
('DG0042', '1705010', '1010404', '1230301', 'Dinda Tasya Rahayu', 2500000, '22', 1000000, 0, 0, 3500000, '2019-06-22'),
('DG0043', '1805007', '1010404', '1230301', 'Siti Zubaidah', 2500000, '22', 1000000, 0, 0, 3500000, '2019-06-22'),
('DG0044', '1606015', '1010405', '1299701', 'Nadya Ayu Febrilia', 2500000, '22', 1000000, 0, 0, 3500000, '2019-06-22'),
('DG0045', '1702005', '1010405', '1299701', 'Endang Pradana', 2500000, '22', 1000000, 0, 0, 3500000, '2019-06-22'),
('DG0046', '1805004', '1010405', '1299701', 'Inem', 2500000, '22', 1000000, 0, 0, 3500000, '2019-06-22'),
('DG0047', '1001001', '1010201', '1219001', 'Asya', 5000000, '22', 3000000, 2000000, 2000000, 12000000, '2019-06-23'),
('DG0048', '1001004', '1010201', '1219001', 'Mustika', 5000000, '22', 3000000, 2000000, 500000, 10500000, '2019-06-23'),
('DG0049', '1001005', '1010201', '1219001', 'Dhea', 5000000, '22', 3000000, 2000000, 1000000, 11000000, '2019-06-23'),
('DG0050', '1001006', '1010201', '1219001', 'Fadhil Herman', 5000000, '22', 3000000, 2000000, 1200000, 11200000, '2019-06-23'),
('DG0051', '1003009', '1010201', '1219001', 'Putri Lestari', 5000000, '22', 3000000, 2000000, 900000, 10900000, '2019-06-23'),
('DG0052', '1004002', '1010201', '1219001', 'Nabilah Fahira', 5000000, '22', 3000000, 2000000, 1500000, 11500000, '2019-06-23'),
('DG0053', '1005004', '1010201', '1219001', 'Akbar Ramadhan', 5000000, '22', 3000000, 2000000, 500000, 10500000, '2019-06-23'),
('DG0054', '1103003', '1010201', '1219001', 'Ahmad Setia', 5000000, '22', 3000000, 2000000, 1000000, 11000000, '2019-06-23'),
('DG0055', '1003014', '1010202', '1284001', 'Ardian Septian', 5000000, '22', 3000000, 1000000, 500000, 9500000, '2019-06-23'),
('DG0056', '1005011', '1010202', '1284001', 'Alfian Hermawan', 5000000, '22', 3000000, 1000000, 700000, 9700000, '2019-06-23'),
('DG0057', '1005013', '1010202', '1284001', 'Dinda Agustin', 5000000, '22', 3000000, 1000000, 800000, 9800000, '2019-06-23'),
('DG0058', '1006003', '1010202', '1284001', 'Dini Aulia', 5000000, '22', 3000000, 1000000, 1700000, 10700000, '2019-06-23'),
('DG0059', '1101007', '1010202', '1284001', 'Rizky Saputra', 5000000, '22', 3000000, 1000000, 750000, 9750000, '2019-06-23'),
('DG0060', '1104011', '1010202', '1284001', 'Nita Safira', 5000000, '22', 3000000, 1000000, 1650000, 10650000, '2019-06-23'),
('DG0061', '1106017', '1010202', '1284001', 'Ahmad Shobari', 5000000, '22', 3000000, 1000000, 1300000, 10300000, '2019-06-23'),
('DG0062', '1203002', '1010202', '1284001', 'Tiara Julia', 5000000, '22', 3000000, 1000000, 1700000, 10700000, '2019-06-23'),
('DG0063', '1002012', '1010203', '1284001', 'Lala Lidya', 5000000, '22', 3000000, 1500000, 1500000, 11000000, '2019-06-23'),
('DG0064', '1005005', '1010203', '1267801', 'Novi Yanna', 5000000, '22', 3000000, 1500000, 1200000, 10700000, '2019-06-23'),
('DG0065', '1007018', '1010203', '1267801', 'Wahyudi Rizal', 5000000, '22', 3000000, 1500000, 800000, 10300000, '2019-06-23'),
('DG0066', '1103008', '1010203', '1267801', 'Ricky Syahbani', 5000000, '22', 3000000, 1500000, 1500000, 11000000, '2019-06-23'),
('DG0067', '1104004', '1010203', '1267801', 'Rizka Ameliani', 5000000, '22', 3000000, 1500000, 2000000, 11500000, '2019-06-23'),
('DG0068', '1203008', '1010203', '1267801', 'Anne Rizky', 5000000, '22', 3000000, 1500000, 1800000, 11300000, '2019-06-23'),
('DG0069', '1304005', '1010203', '1267801', 'Maulidya', 5000000, '22', 3000000, 1500000, 1550000, 11050000, '2019-06-23'),
('DG0070', '1305009', '1010203', '1267801', 'Zakaria Hafizh', 5000000, '22', 3000000, 1500000, 900000, 10400000, '2019-06-23'),
('DG0071', '1005017', '1010204', '1230301', 'Ryan Firdaus', 5000000, '22', 3000000, 1300000, 900000, 10200000, '2019-06-23'),
('DG0072', '1006009', '1010204', '1230301', 'Afri Dewita', 5000000, '22', 3000000, 1500000, 1500000, 11000000, '2019-06-23'),
('DG0073', '1102002', '1010204', '1230301', 'Lisa Afriani', 5000000, '22', 3000000, 1400000, 1250000, 10650000, '2019-06-23'),
('DG0074', '1103005', '1010204', '1230301', 'Anatasya Nurul Debora', 5000000, '22', 3000000, 1500000, 1250000, 10750000, '2019-06-23'),
('DG0075', '1204008', '1010204', '1230301', 'Robby Alkausar', 5000000, '22', 3000000, 1200000, 850000, 10050000, '2019-06-23'),
('DG0076', '1206014', '1010204', '1230301', 'Adelia Oktaviani', 5000000, '22', 3000000, 1500000, 1700000, 11200000, '2019-06-23'),
('DG0077', '1304015', '1010204', '1230301', 'Luthfi Syarif Pratama', 5000000, '22', 3000000, 1400000, 800000, 10200000, '2019-06-23'),
('DG0078', '1309003', '1010204', '1230301', 'Yogi Chandra', 5000000, '22', 3000000, 1500000, 500000, 10000000, '2019-06-23'),
('DG0079', '1207001', '1010205', '1299701', 'Adi Pradana', 5000000, '22', 3000000, 2000000, 1200000, 11200000, '2019-06-23'),
('DG0080', '1308018', '1010205', '1299701', 'Ditha Zettira', 5000000, '22', 3000000, 1700000, 1500000, 11200000, '2019-06-23'),
('DG0081', '1309009', '1010205', '1299701', 'Amelia Dwi Yanti', 5000000, '22', 3000000, 1500000, 1500000, 11000000, '2019-06-23'),
('DG0082', '1402019', '1010205', '1299701', 'Iqbal Ramadhan', 5000000, '22', 3000000, 1500000, 750000, 10250000, '2019-06-23'),
('DG0083', '1503017', '1010205', '1299701', 'Syarifah Melinda', 5000000, '22', 3000000, 1500000, 1700000, 11200000, '2019-06-23'),
('DG0084', '1505015', '1010205', '1299701', 'Yoga Ragilliasto', 5000000, '22', 3000000, 1500000, 1500000, 11000000, '2019-06-23'),
('DG0085', '1606001', '1010205', '1299701', 'Muhammad Fadhly', 5000000, '22', 3000000, 1500000, 500000, 10000000, '2019-06-23');

-- --------------------------------------------------------

--
-- Table structure for table `tbljabatan`
--

CREATE TABLE `tbljabatan` (
  `id_jabatan` varchar(20) NOT NULL,
  `id_divisi` varchar(20) NOT NULL,
  `nama_jabatan` varchar(30) NOT NULL,
  `gaji_pokok` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbljabatan`
--

INSERT INTO `tbljabatan` (`id_jabatan`, `id_divisi`, `nama_jabatan`, `gaji_pokok`) VALUES
('1010101', '1219001', 'Supervisor', '10000000'),
('1010102', '1284001', 'Supervisor', '10000000'),
('1010103', '1267801', 'Supervisor', '10000000'),
('1010104', '1230301', 'Supervisor', '10000000'),
('1010105', '1299701', 'Supervisor', '10000000'),
('1010201', '1284001', 'Staff', '5000000'),
('1010202', '1219001', 'Staff', '5000000'),
('1010203', '1267801', 'Staff', '5000000'),
('1010204', '1230301', 'Staff', '5000000'),
('1010205', '1299701', 'Staff', '5000000'),
('1010301', '1284001', 'Direktur', '28000000'),
('1010302', '1267801', 'Direktur', '28000000'),
('1010303', '1219001', 'Direktur', '28000000'),
('1010304', '1230301', 'Direktur', '28000000'),
('1010305', '1299701', 'Direktur', '28000000'),
('1010401', '1284001', 'Office Boy', '2500000'),
('1010402', '1267801', 'Office Boy', '2500000'),
('1010403', '1230301', 'Office Boy', '2500000'),
('1010404', '1219001', 'Office Boy', '2500000'),
('1010405', '1299701', 'Office Boy', '2500000'),
('1010501', '1284001', 'Wakil Direktur', '23000000'),
('1010502', '1267801', 'Wakil Direktur', '23000000'),
('1010503', '1230301', 'Wakil Direktur', '23000000'),
('1010504', '1299701', 'Wakil Direktur', '23000000'),
('1010505', '1219001', 'Wakil Direktur', '23000000'),
('1012501', '1284001', 'Manager', '13000000'),
('1012502', '1267801', 'Manager', '13000000'),
('1012503', '1230301', 'Manager', '13000000'),
('1012504', '1299701', 'Manager', '13000000'),
('1012505', '1219001', 'Manager', '13000000'),
('1013010', '1284001', 'Sekretaris', '8000000'),
('1013011', '1267801', 'Sekretaris', '8000000'),
('1013012', '1219001', 'Sekretaris', '8000000'),
('1013013', '1230301', 'Sekretaris', '8000000'),
('1013014', '1299701', 'Sekretaris', '8000000');

-- --------------------------------------------------------

--
-- Table structure for table `tblpegawai`
--

CREATE TABLE `tblpegawai` (
  `id_pegawai` varchar(20) NOT NULL,
  `id_divisi` varchar(50) NOT NULL,
  `id_jabatan` varchar(20) NOT NULL,
  `nama` varchar(30) NOT NULL,
  `jenkel` varchar(10) NOT NULL,
  `alamat` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblpegawai`
--

INSERT INTO `tblpegawai` (`id_pegawai`, `id_divisi`, `id_jabatan`, `nama`, `jenkel`, `alamat`) VALUES
('0805001', '1219001', '1010303', 'Maulana Malik', 'laki-laki', 'Tebet , Jakarta-Selatan'),
('0805002', '1284001', '1010301', 'Alisya Calista', 'perempuan', 'Kelapa Gading , Jakarta-Utara'),
('0805003', '1267801', '1010302', 'Angelina Augustia', 'perempuan', 'Menteng , Jakarta-Pusat'),
('0805004', '1230301', '1010304', 'Alvaro Quilan', 'laki-laki', 'Blok-M , Jakarta-Selatan'),
('0805005', '1299701', '1010305', 'Rini Aulia', 'perempuan', 'Tebet , Jakarta-Selatan'),
('0807001', '1219001', '1010501', 'Ajeng Puspita', 'perempuan', 'Depok , Jawa Barat'),
('0807002', '1284001', '1010502', 'Putri Damayanti', 'perempuan', 'Mampang , Jakarta-Selatan'),
('0807003', '1267801', '1010503', 'Kurniawan ', 'laki-laki', 'Bogor , Jawa Barat'),
('0807004', '1230301', '1010504', 'Hadi', 'laki-laki', 'Cileungsi , Jawa Barat'),
('0807005', '1299701', '1010505', 'Febrilio', 'laki-laki', 'Jonggol , Jawa Barat'),
('0807006', '0', '1012501', 'Ahmad Mauludy', 'laki-laki', 'Cilandak , Jakarta-Selatan'),
('0807007', '0', '1012502', 'Fandi Bastian', 'laki-laki', 'Bintara , Bekasi-Barat'),
('0807008', '0', '1012503', 'Reza Pratama', 'laki-laki', 'Condet , Jakarta-Timur'),
('0807009', '0', '1012504', 'Carissa Nasution', 'perempuan', 'Bangka , Jakarta-Selatan'),
('0807010', '0', '1012505', 'Suci Salsabilah', 'perempuan', 'Tebet , Jakarta-Selatan'),
('0807011', '0', '1010101', 'Ahmad Setyo', 'laki-laki', 'Kp Melayu , Jakarta-Timur'),
('0807012', '0', '1010102', 'Fauziah Ramadhania', 'perempuan', 'Pisangan Lama , Jakarta-Timur'),
('0807013', '0', '1010103', 'Fauzan Utama', 'laki-laki', 'Otista , Jakarta-Timur'),
('0807014', '0', '1010104', 'Clara Saputri', 'perempuan', 'Blok-M , Jakarta-Selatan'),
('0807015', '0', '1010105', 'Hilda Rahayu', 'perempuan', 'Menteng Dalam , Jakarta-Selatan'),
('0810001', '0', '1013010', 'Agatha Oktaviani', 'perempuan', 'Menteng Dalam , Jakarta-Selatan'),
('0810002', '0', '1013011', 'Yulianti Fadhilah', 'perempuan', 'Menteng Wadas , Jakarta-Selatan'),
('0810003', '0', '1013012', 'Meri Wijaya', 'perempuan', 'Setia Budi , Jakarta-Selatan'),
('0810004', '0', '1013013', 'Alya Dwi Aryanti', 'perempuan', 'Kemang , Jakarta-Selatan'),
('0810005', '0', '1013014', 'Feby Anggraini Putri', 'perempuan', 'Kemang , Jakarta-Selatan'),
('1001001', '0', '1010201', 'Asya', 'perempuan', 'Pengadegan , Jakarta-Selata'),
('1001004', '0', '1010201', 'Mustika', 'perempuan', 'Manggarai , Jakarta-Selatan'),
('1001005', '0', '1010201', 'Dhea', 'perempuan', 'Kayu Manis , Jakarta-Timur10'),
('1001006', '0', '1010201', 'Fadhil Herman', 'laki-laki', 'Jagakarsa , Jakarta-Selatan'),
('1002012', '0', '1010203', 'Lala Lidya', 'perempuan', 'Bangka , Jakarta-Selatan'),
('1003009', '0', '1010201', 'Putri Lestari', 'perempuan', 'Pondok Bambu , Jakarta-Timur'),
('1003014', '0', '1010202', 'Ardian Septian', 'laki-laki', 'Pengadegan , Jakarta-Selatan'),
('1004002', '0', '1010201', 'Nabilah Fahira', 'perempuan', 'Kelapa Gading , Jakarta-Utara'),
('1005004', '0', '1010201', 'Akbar Ramadhan', 'laki-laki', 'Pondok Gede , Jakarta-Timur'),
('1005005', '0', '1010203', 'Novi Yanna', 'perempuan', 'Cipinang Besar , Jakarta-Timur'),
('1005011', '0', '1010202', 'Alfian Hermawan', 'laki-laki', 'Manggarai , Jakarta-Selatan'),
('1005013', '0', '1010202', 'Dinda Agustin', 'perempuan', 'Duren Tiga, Jakarta-Selatan'),
('1005017', '0', '1010204', 'Ryan Firdaus', 'laki-laki', 'Pasar Minggu , Jakarta-Selatan'),
('1006003', '0', '1010202', 'Dini Aulia', 'perempuan', 'Kalibata , Jakarta-Selatan'),
('1006009', '0', '1010204', 'Afri Dewita', 'perempuan', 'Pejaten , Jakarta-Selatan'),
('1007018', '0', '1010203', 'Wahyudi Rizal', 'laki-laki', 'Cawang , Jakarta-Timur'),
('1101007', '0', '1010202', 'Rizky Saputra', 'laki-laki', 'Setia Budi , Jakarta-Selatan'),
('1102002', '0', '1010204', 'Lisa Afriani', 'perempuan', 'Pejaten , Jakarta-Selatan'),
('1103003', '0', '1010201', 'Ahmad Setia', 'laki-laki', 'Karet , Jakarta-Selatan'),
('1103005', '0', '1010204', 'Anatasya Nurul Debora', 'perempuan', 'Palmerah , Jakarta-Selatan'),
('1103008', '0', '1010203', 'Ricky Syahbani', 'laki-laki', 'Kp Pulo , Jakarta-Timur'),
('1104004', '0', '1010203', 'Rizka Ameliani', 'perempuan', 'Kuningan , Jakarta-Selatan'),
('1104011', '0', '1010202', 'Nita Safira', 'perempuan', 'Duren Tiga , Jakarta-Selatan'),
('1106017', '0', '1010202', 'Ahmad Shobari', 'laki-laki', 'Kebon Baru , Jakarta-Selatan'),
('1203002', '0', '1010202', 'Tiara Julia', 'perempuan', 'Veteran , Jakarta-Selatan'),
('1203008', '0', '1010203', 'Anne Rizky', 'perempuan', 'Kemang , Jakarta-Selatan'),
('1204008', '0', '1010204', 'Robby Alkausar', 'laki-laki', 'Blok M , Jakarta-Selatan'),
('1206014', '0', '1010204', 'Adelia Oktaviani', 'perempuan', 'Menteng Dalam , Jakarta-Selatan'),
('1207001', '0', '1010205', 'Adi Pradana', 'laki-laki', 'Ciracas , Jakarta-Timur'),
('1304005', '0', '1010203', 'Maulidya', 'perempuan', 'Kemang , Jakarta-Selatan'),
('1304015', '0', '1010204', 'Luthfi Syarif Pratama', 'laki-laki', 'Pondok Bambu , Jakarta-Timur'),
('1305009', '0', '1010203', 'Zakaria Hafizh', 'laki-laki', 'Pancoran , Jakarta-Selatan'),
('1308018', '0', '1010205', 'Ditha Zettira', 'perempuan', 'Kebayoran Lama , Jakarta-Selatan'),
('1309003', '0', '1010204', 'Yogi Chandra', 'laki-laki', 'Bendungan Hilir , Jakarta-Pusat'),
('1309009', '0', '1010205', 'Amelia Dwi Yanti', 'perempuan', 'Kemang , Jakarta-Selatan'),
('1402019', '0', '1010205', 'Iqbal Ramadhan', 'laki-laki', 'Dukuh Atas , Jakarta-Pusat'),
('1503017', '0', '1010205', 'Syarifah Melinda', 'perempuan', 'Kebayoran Baru , Jakarta-Selatan'),
('1505015', '0', '1010205', 'Yoga Ragilliasto', 'laki-laki', 'Tebet , Jakarta-Selatan'),
('1604009', '0', '1010401', 'Yuni Audia', 'perempuan', 'Manggarai , Jakarta-Selatan'),
('1605005', '0', '1010402', 'Nugroho Seto Pamungkas', 'laki-laki', 'Setia Budi , Jakarta-Selatan'),
('1606001', '0', '1010205', 'Muhammad Fadhly', 'laki-laki', 'Menteng , Jakarta-Pusat'),
('1606015', '0', '1010405', 'Nadya Ayu Febrilia', 'perempuan', 'Bukit Duri , Jakarta-Selatan'),
('1608003', '0', '1010402', 'Bayu Permana', 'laki-laki', 'Bukit Duri , Jakarta-Selatan'),
('1702005', '0', '1010405', 'Endang Pradana', 'laki-laki', 'Menteng Atas , Jakarta-Selatan'),
('1703001', '0', '1010403', 'Ahmad Ghafary', 'laki-laki', 'Tomang , Jakarta-Barat'),
('1703009', '0', '1010404', 'Erlangga Hertanto', 'laki-laki', 'Percetakan Negara , Jakarta-Pusat'),
('1704008', '0', '1010404', 'Gina Putri Cempaka', 'perempuan', 'Karet , Jakarta-Selatan'),
('1705007', '0', '1010403', 'Aldy Pamungkas', 'laki-laki', 'Bukit Duri , Jakarta-Selatan'),
('1705010', '0', '1010404', 'Dinda Tasya Rahayu', 'perempuan', 'Kayu Manis , Jakarta-Timur'),
('1707003', '0', '1010403', 'Ayu Ningtyas', 'perempuan', 'Utan Kayu , Jakarta-Timur'),
('1709001', '0', '1010401', 'Joko Santoso', 'laki-laki', 'Kp.Melayu , Jakarta-Timur'),
('1802008', '0', '1010403', 'Muhammad Ridho Aditya', 'laki-laki', 'Cipinang Muara , Jakarta-Timur'),
('1802011', '0', '1010402', 'Rizky Daud Saputra', 'laki-laki', 'Cipinang Muara , Jakarta-Timur'),
('1804010', '0', '1010401', 'Andi Permana', 'laki-laki', 'Percetakan Negara , Jakarta-Pusat'),
('1804018', '0', '1010401', 'Pratama Putra', 'laki-laki', 'Bukit Duri , Jakarta-Selatan'),
('1805004', '0', '1010405', 'Inem', 'perempuan', 'Kwitang , Jakarta-Pusat'),
('1805007', '0', '1010404', 'Siti Zubaidah', 'perempuan', 'Cipinang Besar , Jakarta-Timur'),
('1809002', '0', '1010403', 'Aditya Suherman', 'laki-laki', 'Tanah Tinggi , Jakarta-Pusat'),
('1809015', '0', '1010401', 'Kokom Zakiah', 'perempuan', 'Paseban , Jakarta-Pusat'),
('1902004', '0', '1010402', 'Jihan Wandayani', 'perempuan', 'Kramat Sawah , Jakarta-Pusat');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `Username` varchar(30) NOT NULL,
  `Password` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`Username`, `Password`) VALUES
('Malik', 'maret');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tblabsensi`
--
ALTER TABLE `tblabsensi`
  ADD PRIMARY KEY (`id_pegawai`);

--
-- Indexes for table `tbldivisi`
--
ALTER TABLE `tbldivisi`
  ADD PRIMARY KEY (`id_divisi`);

--
-- Indexes for table `tblgaji`
--
ALTER TABLE `tblgaji`
  ADD PRIMARY KEY (`id_slipgaji`);

--
-- Indexes for table `tbljabatan`
--
ALTER TABLE `tbljabatan`
  ADD PRIMARY KEY (`id_jabatan`);

--
-- Indexes for table `tblpegawai`
--
ALTER TABLE `tblpegawai`
  ADD PRIMARY KEY (`id_pegawai`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`Username`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
