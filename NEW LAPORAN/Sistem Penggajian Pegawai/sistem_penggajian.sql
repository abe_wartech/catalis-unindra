-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 26, 2020 at 07:20 AM
-- Server version: 5.5.39
-- PHP Version: 5.4.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `sistem_penggajian`
--

-- --------------------------------------------------------

--
-- Table structure for table `absensi`
--

CREATE TABLE IF NOT EXISTS `absensi` (
  `bulan` varchar(20) NOT NULL,
  `id_absensi` varchar(15) NOT NULL,
  `id_pegawai` varchar(15) NOT NULL,
  `nama_pegawai` varchar(30) NOT NULL,
  `jenis_kelamin` varchar(15) NOT NULL,
  `notelp` varchar(15) NOT NULL,
  `jabatan` varchar(30) NOT NULL,
  `kehadiran` int(20) NOT NULL,
  `tidak_hadir` int(20) NOT NULL,
  `total_jam_lembur` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `absensi`
--

INSERT INTO `absensi` (`bulan`, `id_absensi`, `id_pegawai`, `nama_pegawai`, `jenis_kelamin`, `notelp`, `jabatan`, `kehadiran`, `tidak_hadir`, `total_jam_lembur`) VALUES
('Februari 2020', 'IA0001', 'IP0002', 'Eka', 'Perempuan', '08555777333', 'Client Services Officer', 22, 5, 5),
('Februari 2020', 'IA0002', 'IP0001', 'Muhammad Rizky Yanwar', 'Laki-Laki', '08999888777', 'Client Services Officer', 25, 5, 8),
('Februari 2020', 'IA0003', 'IP0002', 'Muhammad Farhat Djehan Kholiq', 'Laki-Laki', '081213141516', 'Office Boy', 23, 4, 7),
('Maret 2020', 'IA0004', 'IP0001', 'Muhammad Rizky Yanwar', 'Laki-Laki', '08999888777', 'Client Services Officer', 22, 1, 9);

-- --------------------------------------------------------

--
-- Table structure for table `jabatan`
--

CREATE TABLE IF NOT EXISTS `jabatan` (
  `id_jabatan` varchar(10) NOT NULL,
  `nama_jabatan` varchar(30) NOT NULL,
  `gaji_pokok` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `jabatan`
--

INSERT INTO `jabatan` (`id_jabatan`, `nama_jabatan`, `gaji_pokok`) VALUES
('IJ0001', 'Client Services Officer', 5000000),
('IJ0002', 'Office Boy', 3000000);

-- --------------------------------------------------------

--
-- Table structure for table `pegawai`
--

CREATE TABLE IF NOT EXISTS `pegawai` (
  `id_pegawai` varchar(10) NOT NULL,
  `nama_pegawai` varchar(30) NOT NULL,
  `jenis_kelamin` varchar(15) NOT NULL,
  `tgl_lahir` varchar(20) NOT NULL,
  `notelp` varchar(15) NOT NULL,
  `jabatan` varchar(30) NOT NULL,
  `alamat` varchar(300) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pegawai`
--

INSERT INTO `pegawai` (`id_pegawai`, `nama_pegawai`, `jenis_kelamin`, `tgl_lahir`, `notelp`, `jabatan`, `alamat`) VALUES
('IP0001', 'Muhammad Rizky Yanwar', 'Laki-Laki', '1998-02-19', '08999888777', 'Client Services Officer', 'JL. WIjaya Kusuma Blok AS.49 No.18 RT/RW 003/011'),
('IP0002', 'Muhammad Farhat Djehan Kholiq', 'Laki-Laki', '1994-04-06', '081213141516', 'Office Boy', 'Grand Depok City, Ruko Anggrek 1 No. 18'),
('IP0003', 'fdsfsd', 'Laki-Laki', '2020-02-04', '54543', 'Office Boy', 'tretr'),
('IP0004', 'grdgfdg', 'Perempuan', '2020-02-08', '54543543', 'Client Services Officer', 'rgdgfgd'),
('IP0005', 'gfdgfdgfd', 'Laki-Laki', '2020-02-07', '545435', 'Client Services Officer', 'gtfhgfdhg'),
('IP0006', 'hgfhvffd', 'Laki-Laki', '2020-02-04', '5t4654653', 'Office Boy', 'tgfdhgfdhfg'),
('IP0007', 'gfhfdhgfd', 'Perempuan', '2020-02-08', '6436543', 'Client Services Officer', 'hgfhgfdhgf'),
('IP0008', 'fgfdgfdgdg', 'Laki-Laki', '2020-02-06', '4643654', 'Client Services Officer', 'gtfgfhgfh'),
('IP0009', 'tretret', 'Laki-Laki', '2020-02-04', '54543543', 'Client Services Officer', 'hgfhgfhgf'),
('IP0010', 'tgdgfdgd', 'Laki-Laki', '2020-02-11', '54543543', 'Office Boy', 'ygrsarfdsgfd'),
('IP0011', 'rerewrwe', 'Laki-Laki', '2020-02-06', '54543543', 'Office Boy', 'grdgfdgfd');

-- --------------------------------------------------------

--
-- Table structure for table `slip_gaji`
--

CREATE TABLE IF NOT EXISTS `slip_gaji` (
  `tanggal` varchar(25) NOT NULL,
  `id_slip_gaji` varchar(15) NOT NULL,
  `bulan` varchar(20) NOT NULL,
  `id_absensi` varchar(15) NOT NULL,
  `id_pegawai` varchar(15) NOT NULL,
  `nama_pegawai` varchar(50) NOT NULL,
  `id_jabatan` varchar(15) NOT NULL,
  `nama_jabatan` varchar(50) NOT NULL,
  `gaji_pokok` varchar(50) NOT NULL,
  `kehadiran` varchar(10) NOT NULL,
  `tidak_hadir` varchar(10) NOT NULL,
  `total_jam_lembur` varchar(10) NOT NULL,
  `total_upah_lembur` varchar(50) NOT NULL,
  `total_gaji` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `slip_gaji`
--

INSERT INTO `slip_gaji` (`tanggal`, `id_slip_gaji`, `bulan`, `id_absensi`, `id_pegawai`, `nama_pegawai`, `id_jabatan`, `nama_jabatan`, `gaji_pokok`, `kehadiran`, `tidak_hadir`, `total_jam_lembur`, `total_upah_lembur`, `total_gaji`) VALUES
('2020-02-21, 15:32:03', 'IG0001', 'Februari 2020', 'IA0001', 'IP0002', 'Eka', 'IJ0001', 'Client Services Officer', 'Rp. 5.000.000,00', '22 Hari', '5 Hari', '5 Jam', 'Rp. 274.559,00', 'Rp. 5.274.559,00'),
('2020-02-25, 11:21:02', 'IG0002', 'Februari 2020', 'IA0001', 'IP0002', 'Eka', 'IJ0001', 'Client Services Officer', 'Rp. 5.000.000,00', '22 Hari', '5 Hari', '5 Jam', 'Rp. 274.559,00', 'Rp. 5.274.559,00'),
('2020-02-25, 11:21:32', 'IG0003', 'Februari 2020', 'IA0001', 'IP0002', 'Eka', 'IJ0001', 'Client Services Officer', 'Rp. 5.000.000,00', '22 Hari', '5 Hari', '5 Jam', 'Rp. 274.559,00', 'Rp. 5.274.559,00'),
('2020-02-25, 11:37:46', 'IG0004', 'Februari 2020', 'IA0001', 'IP0002', 'Eka', 'IJ0001', 'Client Services Officer', 'Rp. 5.000.000,00', '22 Hari', '5 Hari', '5 Jam', 'Rp. 274.559,00', 'Rp. 5.274.559,00'),
('2020-02-25, 11:38:39', 'IG0005', 'Februari 2020', 'IA0001', 'IP0002', 'Eka', 'IJ0001', 'Client Services Officer', 'Rp. 5.000.000,00', '22 Hari', '5 Hari', '5 Jam', 'Rp. 274.559,00', 'Rp. 5.274.559,00'),
('2020-02-25, 11:40:16', 'IG0006', 'Februari 2020', 'IA0001', 'IP0002', 'Eka', 'IJ0001', 'Client Services Officer', '5000000', '22 Hari', '5 Hari', '5 Jam', 'Rp. 274.559,00', 'Rp. 5.274.559,00'),
('2020-02-25, 12:04:13', 'IG0007', 'Februari 2020', 'IA0001', 'IP0002', 'Eka', 'IJ0001', 'Client Services Officer', 'Rp. 5.000.000,00', '22 Hari', '5 Hari', '5 Jam', 'Rp. 274.559,00', 'Rp. 5.274.559,00'),
('2020-02-25, 12:05:39', 'IG0008', 'Februari 2020', 'IA0001', 'IP0002', 'Eka', 'IJ0001', 'Client Services Officer', '5000000', '22 Hari', '5 Hari', '5 Jam', 'Rp. 164.739,00', 'Rp. 3.164.739,00'),
('2020-02-25, 12:06:52', 'IG0009', 'Februari 2020', 'IA0001', 'IP0002', 'Eka', 'IJ0001', 'Client Services Officer', 'Rp. 5.000.000,00', '22 Hari', '5 Hari', '5 Jam', 'Rp. 274.559,00', 'Rp. 5.274.559,00'),
('2020-02-25, 12:13:30', 'IG0010', 'Februari 2020', 'IA0001', 'IP0002', 'Eka', 'IJ0001', 'Client Services Officer', 'Rp. 5.000.000,00', '22 Hari', '5 Hari', '5 Jam', 'Rp. 274.559,00', 'Rp. 5.274.559,00'),
('2020-02-25, 12:16:56', 'IG0011', 'Februari 2020', 'IA0001', 'IP0002', 'Eka', 'IJ0001', 'Client Services Officer', 'Rp. 5.000.000,00', '22 Hari', '5 Hari', '5 Jam', 'Rp. 274.559,00', 'Rp. 5.274.559,00'),
('2020-02-25, 12:48:04', 'IG0012', 'Februari 2020', 'IA0001', 'IP0002', 'Eka', 'IJ0001', 'Client Services Officer', 'Rp. 5.000.000,00', '22 Hari', '5 Hari', '5 Jam', 'Rp. 274.559,00', 'Rp. 5.274.559,00'),
('2020-02-25, 12:52:11', 'IG0013', 'Februari 2020', 'IA0001', 'IP0002', 'Eka', 'IJ0001', 'Client Services Officer', 'Rp. 5.000.000,00', '22 Hari', '5 Hari', '5 Jam', 'Rp. 274.559,00', 'Rp. 5.274.559,00'),
('2020-02-25, 12:52:45', 'IG0014', 'Februari 2020', 'IA0001', 'IP0002', 'Eka', 'IJ0001', 'Client Services Officer', 'Rp. 5.000.000,00', '22 Hari', '5 Hari', '5 Jam', 'Rp. 274.559,00', 'Rp. 5.274.559,00'),
('2020-02-25, 13:11:47', 'IG0015', 'Februari 2020', 'IA0001', 'IP0002', 'Eka', 'IJ0002', 'Office Boy', 'Rp. 3.000.000,00', '22 Hari', '5 Hari', '5 Jam', 'Rp. 164.739,00', 'Rp. 3.164.739,00'),
('2020-02-26, 10:47:36', 'IG0016', 'Maret 2020', 'IA0004', 'IP0001', 'Muhammad Rizky Yanwar', 'IJ0001', 'Client Services Officer', 'Rp. 5.000.000,00', '22 Hari', '1 Hari', '9 Jam', 'Rp. 505.767,00', 'Rp. 5.505.767,00'),
('2020-02-26, 11:16:29', 'IG0017', 'Februari 2020', 'IA0003', 'IP0002', 'Muhammad Farhat Djehan Kholiq', 'IJ0002', 'Office Boy', 'Rp. 3.000.000,00', '23 Hari', '4 Hari', '7 Jam', 'Rp. 234.103,00', 'Rp. 3.234.103,00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `absensi`
--
ALTER TABLE `absensi`
 ADD PRIMARY KEY (`id_absensi`);

--
-- Indexes for table `jabatan`
--
ALTER TABLE `jabatan`
 ADD PRIMARY KEY (`id_jabatan`);

--
-- Indexes for table `pegawai`
--
ALTER TABLE `pegawai`
 ADD PRIMARY KEY (`id_pegawai`);

--
-- Indexes for table `slip_gaji`
--
ALTER TABLE `slip_gaji`
 ADD PRIMARY KEY (`id_slip_gaji`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
